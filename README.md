# Springboot Redis Cache 
## 实现对 Spring Cache扩展：Cacheable注解失效时间+小于某时间主动刷新缓存

* Spring Cache默认是不支持在@Cacheable上添加过期时间的,所以实现扩展
  * 缓存失效时间支持在方法的注解上指定
  * 小于某时间主动刷新缓存也支持在方法的注解上指定
* 例子：  @Cacheable(value = {"user#10#5"},key ="'pre'+#id")
  这里过期时间为10秒，剩余5秒会触发主动刷新缓存，由于key是带前缀，所以做了些通配
* 参考blog：http://www.cnblogs.com/ASPNET2008/p/6511500.html
 
  * ### 技术架构：
  * springboot做Restful Api和控制反转
  * mybatis做持久化
  * druid做数据源并监控请求 ```http://localhost:8888/druid/login.html``` 账号密码看application.properties
  * postgreSql做数据库
  * redis做缓存
  * swagger做Restful Api文档的查看 ```http://localhost:8888/swagger-ui.html```
  * logback做日志输出