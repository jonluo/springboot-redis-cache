package com.jonluo.cache.config;

import com.jonluo.cache.redis.manager.CustomizedRedisCacheManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

/**
 *  redis cache 配置
 * @author jonluo
 * @date 2018/3/28 16:30
 */
@Configuration
public class RedisCacheConfig {

    @Value("${redis.cache.default-expire-time}")
    private Long defaultExpireTime;
    /**
     *  配置缓存bean
     * @author luojuwen
     * @date 2018/3/28 16:21
     * @param  redisTemplate
     * @return
     */
    @Bean
    public CacheManager cacheManager(@SuppressWarnings("rawtypes") RedisTemplate redisTemplate) {
        CustomizedRedisCacheManager cacheManager= new CustomizedRedisCacheManager(redisTemplate);
        //默认半个小时超时时间
        cacheManager.setDefaultExpiration(defaultExpireTime);
        return cacheManager;
    }

}
