package com.jonluo.cache.redis.support;

import java.lang.reflect.Method;
import java.util.Set;

/**
 * 手动刷新缓存和注册接口
 * @author luojuwen
 * @date 2018/3/28 14:58
 */
public interface CacheSupport {

	/**
	 *   注册
	 * @author luojuwen
	 * @date 2018/3/28 14:55
	 * @param   invokedBean
	 * @param   invokedMethod
	 * @param   invocationArguments
	 * @param   cacheNames
	 * @param   preKey
	 * @return
	 */
	void registerInvocation(Object invokedBean, Method invokedMethod, Object[] invocationArguments, Set<String> cacheNames,String preKey);


	/**
	 * 按容器以及指定键更新缓存
	 * @param cacheName
	 * @param cacheKey
     */
	void refreshCacheByKey(String cacheName, String cacheKey);

}