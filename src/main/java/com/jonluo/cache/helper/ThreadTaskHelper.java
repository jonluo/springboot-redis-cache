package com.jonluo.cache.helper;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.*;

/**
 * 任务线程池
 * @author jonluo
 * @date 2018/3/28 14:56
 */
public class ThreadTaskHelper {

    private static ExecutorService executorService= Executors.newFixedThreadPool(20);

    public static void run(Runnable runnable){
        executorService.execute(runnable);
    }
}
