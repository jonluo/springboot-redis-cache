package com.jonluo.cache.helper;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 获取应用上下文
 * @author jonluo
 * @date 2018/3/28 14:56
 */
@Component
public class ApplicationContextHelper implements ApplicationContextAware {

	private static ApplicationContext ctx;

	@Override
	synchronized public void setApplicationContext(ApplicationContext appContext)
			throws BeansException {
		ctx = appContext;

	}

	public static ApplicationContext getApplicationContext() {
		return ctx;
	}
}
