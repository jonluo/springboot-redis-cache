package com.jonluo.demo.exception;


import com.jonluo.demo.utils.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 *
 * @author jonluo
 * Date 2017/9/15
 * 异常处理器
 */
@RestControllerAdvice
public class ResultReturnExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 自定义异常
	 */
	@ExceptionHandler(ResultReturnException.class)
	public Result handleRRException(ResultReturnException e){
		logger.error(e.getMessage(), e);
		return  Result.error(e.getCode(),e.getMessage());
	}

	@ExceptionHandler(Exception.class)
	public Result handleException(Exception e){
		logger.error(e.getMessage(), e);
		return  Result.error(500,e.getMessage());
	}
}
