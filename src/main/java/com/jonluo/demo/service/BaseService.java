package com.jonluo.demo.service;

import com.jonluo.demo.utils.PageInfo;

import java.util.List;

/**
 * @author jonluo
 * Date 2017/9/15
 */
public interface BaseService<T>{

    /**
     *    添加
     * @author luojuwen
     * @date 2018/3/27 17:08
     * @param  t
     * @return
     */
    int save(T t);

    /**
     *    批量添加
     * @author luojuwen
     * @date 2018/3/27 17:09
     * @param  list
     * @return
     */
    int saveBatch(List<T> list);

    /**
     *   更新
     * @author luojuwen
     * @date 2018/3/27 17:09
     * @param  t
     * @return
     */
    int update(T t);

    /**
     *    根据id删除
     * @author luojuwen
     * @date 2018/3/27 17:09
     * @param  id
     * @return
     */
    int delete(String id);

    /**
     *    批量删除
     * @author luojuwen
     * @date 2018/3/27 17:09
     * @param   ids
     * @return
     */
    int deleteBatch(String[] ids);

    /**
     *    根据id查询
     * @author luojuwen
     * @date 2018/3/27 17:09
     * @param  id
     * @return
     */
    T query(String id);

    /**
     *    分页查询
     * @author luojuwen
     * @date 2018/3/27 17:09
     * @param  pageInfo
     * @return
     */
    List<T> queryList(PageInfo pageInfo);

    /**
     *    总数
     * @author luojuwen
     * @date 2018/3/27 17:09
     * @param   pageInfo
     * @return
     */
    Long queryTotal(PageInfo pageInfo);

    /**
     *    总数
     * @author luojuwen
     * @date 2018/3/27 17:09
     * @return
     */
    Long queryTotal();

}
