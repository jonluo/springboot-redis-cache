package com.jonluo.demo.service;

import com.jonluo.demo.model.SysUser;

/**
 * @author jonluo
 * Date 2017/9/15
 */
public interface SysUserService extends BaseService<SysUser> {

}
