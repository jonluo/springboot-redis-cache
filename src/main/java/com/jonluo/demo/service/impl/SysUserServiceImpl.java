package com.jonluo.demo.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.jonluo.demo.mapper.SysUserMapper;
import com.jonluo.demo.model.Example;
import com.jonluo.demo.model.SysUser;
import com.jonluo.demo.service.SysUserService;
import com.jonluo.demo.utils.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author jonluo
 * Date 2017/9/15
 */
@Service("sysUserService")
public class SysUserServiceImpl implements SysUserService {


    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Caching(evict = {@CacheEvict(value = {"userList", "userCount"}, allEntries = true)})
    public int save(SysUser sysUser) {
        return sysUserMapper.insertSelective(sysUser);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Caching(evict = {@CacheEvict(value = {"userList", "userCount"}, allEntries = true)})
    public int saveBatch(List<SysUser> list) {
        int i = 0;
        for (SysUser sysUser : list) {
            i += save(sysUser);
        }

        return i;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Caching(evict = {@CacheEvict(value = {"userList"}, allEntries = true),
            @CacheEvict(value = "user", key = "'userId'+#p0.id")})
    public int update(SysUser sysUser) {
        return sysUserMapper.updateByPrimaryKeySelective(sysUser);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Caching(evict = {@CacheEvict(value = {"userList", "userCount","user"}, allEntries = true)})
    public int delete(String id) {
        return sysUserMapper.deleteByPrimaryKey(Long.parseLong(id));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Caching(evict = {@CacheEvict(value = {"userList", "userCount","user"}, allEntries = true)})
    public int deleteBatch(String[] ids) {
        Example example = new Example();
        Example.Criteria criteria = example.createCriteria();
        int i = 0;
        for (i = 0; i <ids.length; i++) {
            sysUserMapper.deleteByPrimaryKey(Long.parseLong(ids[i]));
        }
        return i;
    }

    @Override
    @Cacheable(value = "user#1800#1750", key = "'userId'+#p0")
    public SysUser query(String id) {

        return sysUserMapper.selectByPrimaryKey(Long.parseLong(id));
    }


    @Override
    @Cacheable(value = "userList#1800#1750", key = "'userList'+#p0.toString()")
    public List<SysUser> queryList(PageInfo pageInfo) {
        return sysUserMapper.selectByExample(pageInfo2Example(pageInfo));
    }

    @Override
    @Cacheable(value = "userCount#1800#1750", key = "'userCount'+#p0.toString()")
    public Long queryTotal(PageInfo pageInfo) {
        return sysUserMapper.countByExample(pageInfo2Example(pageInfo));
    }

    @Override
    @Cacheable(value = "userCount#1800#1750", key = "'userCount'")
    public Long queryTotal() {
        return sysUserMapper.countByExample(null);
    }


    /**
     * @param pageInfo 分页参数转为条件封装example
     * @return example
     */
    private Example pageInfo2Example(PageInfo pageInfo) {
        Example example = new Example();
        Example.Criteria criteria = example.createCriteria();
        //分页
        if (pageInfo.getOffset() >= 0 && pageInfo.getLimit() > 0) {
            example.setOffset(pageInfo.getOffset());
            example.setLimit(pageInfo.getLimit());
        }
        //查询条件
        JSONObject json = pageInfo.getJsonParams();

        String username = json.getString("username");
        if (StringUtils.isNotBlank(username)) {
            criteria.andLike("username", "%" +username + "%");
        }

        return example;
    }
}
