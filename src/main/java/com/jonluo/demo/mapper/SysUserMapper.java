package com.jonluo.demo.mapper;

import com.jonluo.demo.model.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @author luojuwen
 * @date 2018/3/27 17:12
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser>{
}