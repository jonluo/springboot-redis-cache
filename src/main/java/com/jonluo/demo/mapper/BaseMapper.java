package com.jonluo.demo.mapper;

import com.jonluo.demo.model.Example;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 * @author luojuwen
 * @date 2018/3/27 17:12
 */
public interface BaseMapper<T> {
    /**
     *  总数
     * @author luojuwen
     * @date 2018/3/27 17:03
     * @param   example
     * @return   Long
     */
    Long countByExample(Example example);

    /**
     *   删除
     * @author luojuwen
     * @date 2018/3/27 17:03
     * @param   example
     * @return
     */
    int deleteByExample(Example example);

    /**
     *   根据id删除
     * @author luojuwen
     * @date 2018/3/27 17:03
     * @param   id
     * @return
     */
    int deleteByPrimaryKey(Long id);

    /**
     *    添加
     * @author luojuwen
     * @date 2018/3/27 17:03
     * @param   record
     * @return
     */
    int insert(T record);

    /**
     *    添加
     * @author luojuwen
     * @date 2018/3/27 17:03
     * @param   record
     * @return
     */
    int insertSelective(T record);

    /**
     *    分页
     * @author luojuwen
     * @date 2018/3/27 17:03
     * @param   example
     * @return
     */
    List<T> selectByExample(Example example);

    /**
     *    根据id查询
     * @author luojuwen
     * @date 2018/3/27 17:03
     * @param  id
     * @return
     */
    T selectByPrimaryKey(Long id);

    /**
     *    根新
     * @author luojuwen
     * @date 2018/3/27 17:06
     * @param   record
     * @param   example
     * @return
     */
    int updateByExampleSelective(@Param("record") T record, @Param("example") Example example);

    /**
     *   更新
     * @author luojuwen
     * @date 2018/3/27 17:06
     * @param   record
     * @param  example
     * @return
     */
    int updateByExample(@Param("record") T record, @Param("example") Example example);

    /**
     *    更新
     * @author luojuwen
     * @date 2018/3/27 17:06
     * @param   record
     * @return
     */
    int updateByPrimaryKeySelective(T record);

    /**
     *  更新
     * @author luojuwen
     * @date 2018/3/27 17:06
     * @param  record
     * @return
     */
    int updateByPrimaryKey(T record);
}