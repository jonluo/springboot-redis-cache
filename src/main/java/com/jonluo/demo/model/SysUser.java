package com.jonluo.demo.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 */
public class SysUser implements Serializable {

    private Long id;

    private String username;

    private String password;

    private String policenumber;

    private String editor;

    private Date edittime;

    private String rolecode;

    private Integer isvalid;

    /**
     * appid
     */
    private String orgcode;

    /**
     * 用户前端界面所对应的界面风格
     */
    private Integer displaystyle;

    private String admincode;

    private String fullname;

    private String phone;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPolicenumber() {
        return policenumber;
    }

    public void setPolicenumber(String policenumber) {
        this.policenumber = policenumber;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public Date getEdittime() {
        return edittime;
    }

    public void setEdittime(Date edittime) {
        this.edittime = edittime;
    }

    public String getRolecode() {
        return rolecode;
    }

    public void setRolecode(String rolecode) {
        this.rolecode = rolecode;
    }

    public Integer getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(Integer isvalid) {
        this.isvalid = isvalid;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public Integer getDisplaystyle() {
        return displaystyle;
    }

    public void setDisplaystyle(Integer displaystyle) {
        this.displaystyle = displaystyle;
    }

    public String getAdmincode() {
        return admincode;
    }

    public void setAdmincode(String admincode) {
        this.admincode = admincode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SysUser other = (SysUser) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUsername() == null ? other.getUsername() == null : this.getUsername().equals(other.getUsername()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getPolicenumber() == null ? other.getPolicenumber() == null : this.getPolicenumber().equals(other.getPolicenumber()))
            && (this.getEditor() == null ? other.getEditor() == null : this.getEditor().equals(other.getEditor()))
            && (this.getEdittime() == null ? other.getEdittime() == null : this.getEdittime().equals(other.getEdittime()))
            && (this.getRolecode() == null ? other.getRolecode() == null : this.getRolecode().equals(other.getRolecode()))
            && (this.getIsvalid() == null ? other.getIsvalid() == null : this.getIsvalid().equals(other.getIsvalid()))
            && (this.getOrgcode() == null ? other.getOrgcode() == null : this.getOrgcode().equals(other.getOrgcode()))
            && (this.getDisplaystyle() == null ? other.getDisplaystyle() == null : this.getDisplaystyle().equals(other.getDisplaystyle()))
            && (this.getAdmincode() == null ? other.getAdmincode() == null : this.getAdmincode().equals(other.getAdmincode()))
            && (this.getFullname() == null ? other.getFullname() == null : this.getFullname().equals(other.getFullname()))
            && (this.getPhone() == null ? other.getPhone() == null : this.getPhone().equals(other.getPhone()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUsername() == null) ? 0 : getUsername().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getPolicenumber() == null) ? 0 : getPolicenumber().hashCode());
        result = prime * result + ((getEditor() == null) ? 0 : getEditor().hashCode());
        result = prime * result + ((getEdittime() == null) ? 0 : getEdittime().hashCode());
        result = prime * result + ((getRolecode() == null) ? 0 : getRolecode().hashCode());
        result = prime * result + ((getIsvalid() == null) ? 0 : getIsvalid().hashCode());
        result = prime * result + ((getOrgcode() == null) ? 0 : getOrgcode().hashCode());
        result = prime * result + ((getDisplaystyle() == null) ? 0 : getDisplaystyle().hashCode());
        result = prime * result + ((getAdmincode() == null) ? 0 : getAdmincode().hashCode());
        result = prime * result + ((getFullname() == null) ? 0 : getFullname().hashCode());
        result = prime * result + ((getPhone() == null) ? 0 : getPhone().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", username=").append(username);
        sb.append(", password=").append(password);
        sb.append(", policenumber=").append(policenumber);
        sb.append(", editor=").append(editor);
        sb.append(", edittime=").append(edittime);
        sb.append(", rolecode=").append(rolecode);
        sb.append(", isvalid=").append(isvalid);
        sb.append(", orgcode=").append(orgcode);
        sb.append(", displaystyle=").append(displaystyle);
        sb.append(", admincode=").append(admincode);
        sb.append(", fullname=").append(fullname);
        sb.append(", phone=").append(phone);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}