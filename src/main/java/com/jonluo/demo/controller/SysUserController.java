package com.jonluo.demo.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jonluo.demo.model.SysUser;
import com.jonluo.demo.utils.PageInfo;
import com.jonluo.demo.utils.Result;
import com.jonluo.demo.exception.ResultReturnException;
import com.jonluo.demo.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author jonluo
 * Date 2017/9/15
 */
@RestController
@RequestMapping(value="/user")
@Api(description = "用户服务api")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    @ApiOperation(value="获取用户列表", notes="分页查询用户",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"page\":1,\"limit\":10,\"params\":" +
            "{}}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/getAll"}, method=RequestMethod.POST)
    public ResponseEntity getAll(@RequestBody JSONObject jsonObject){
            Result result = null;
            PageInfo pageInfo =  new PageInfo(jsonObject);
            JSONObject json = new JSONObject();
            List<SysUser> list  = sysUserService.queryList(pageInfo);
            Long total = sysUserService.queryTotal(pageInfo);
            json.put("total", total);
            json.put("data", list);
            result = Result.ok(json);
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value="获取用户", notes="根据id查询用户",response = Map.class )
    @ApiImplicitParam(name = "id", value = "用户id",paramType = "path",required = true, dataType = "String")
    @RequestMapping(value={"/getById/{id}"}, method=RequestMethod.GET)
    public ResponseEntity getById(@PathVariable String id){
        Result result = null;
        JSONObject json = new JSONObject();
        SysUser sysUser = sysUserService.query(id);
        json.put("data", sysUser);
        result = Result.ok(json);
        return ResponseEntity.ok(result);
    }


    @ApiOperation(value="添加用户", notes="添加用户信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 {\"id\": 326,\n" +
            "    \"username\": \"zhangdawei\",\n" +
            "    \"password\": \"58074a916b8a65dae507f027f509ae45\",\n" +
            "    \"policenumber\": \"user002\",\n" +
            "    \"editor\": \"admin\",\n" +
            "    \"edittime\": 1519716848000,\n" +
            "    \"rolecode\": \"1001\",\n" +
            "    \"isvalid\": 1,\n" +
            "    \"orgcode\": \"fw-0001\",\n" +
            "    \"displaystyle\": 0,\n" +
            "    \"admincode\": \"17\",\n" +
            "    \"fullname\": \" vvv\",\n" +
            "    \"phone\": \"18344299049\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/add"}, method=RequestMethod.POST)
    public ResponseEntity add(@RequestBody JSONObject jsonObject){
            Result result = null;
            SysUser sysUser = JSON.parseObject(jsonObject.toJSONString(), SysUser.class);
//            ValidatorUtils.validateEntity(sysUser); //后台校验
            Integer sum = sysUserService.save(sysUser);
            if (sum > 0){
                result = Result.ok();
            }else {
                throw new ResultReturnException("添加用户失败");
            }
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value="修改用户", notes="根据id修改用户信息",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 {\"id\": 326,\n" +
            "    \"username\": \"zhangdawei\",\n" +
            "    \"password\": \"58074a916b8a65dae507f027f509ae45\",\n" +
            "    \"policenumber\": \"user002\",\n" +
            "    \"editor\": \"admin\",\n" +
            "    \"edittime\": 1519716848000,\n" +
            "    \"rolecode\": \"1001\",\n" +
            "    \"isvalid\": 1,\n" +
            "    \"orgcode\": \"fw-0001\",\n" +
            "    \"displaystyle\": 0,\n" +
            "    \"admincode\": \"17\",\n" +
            "    \"fullname\": \" vvv\",\n" +
            "    \"phone\": \"18344299049\"}", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/update"}, method=RequestMethod.POST)
    public ResponseEntity update(@RequestBody JSONObject jsonObject){
            Result result = null;
            SysUser sysUser = JSON.parseObject(jsonObject.toJSONString(), SysUser.class);
//            ValidatorUtils.validateEntity(sysUser); //后台校验
            Integer sum = sysUserService.update(sysUser);
            if (sum > 0){
                result = Result.ok();
            }else {
                throw new ResultReturnException("更新用户失败");
            }
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value="删除用户", notes="批量删除用户",response = Map.class )
    @ApiImplicitParam(name = "jsonObject", value = "json对象参数 例子{\"userIds\":\"id1,id2\"}多个id以逗号隔开", required = true, dataType = "JsonObject")
    @RequestMapping(value={"/deleteByIds"}, method=RequestMethod.POST)
    public ResponseEntity deleteByIds(@RequestBody JSONObject jsonObject){
            Result result = null;
            String[] ids= jsonObject.getString("userIds").split(",");
            Integer sum = sysUserService.deleteBatch(ids);
            if (sum == ids.length){
                result = Result.ok();
            }else {
                throw new ResultReturnException("批量删除用户失败");
            }
        return ResponseEntity.ok(result);
    }


}
