package com.jonluo.demo.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 *
 * @author jonluo
 * Date 2017/9/15
 * 分页查询参数
 *
 */
public class PageInfo {

	/**
	 *    当前页码
	 */
    private int page;
    /**
     *   每页条数
     */
    private int limit;
    /**
     *    开始序号
     */
    private int offset;
    /**
     *    查询参数
     */
    private JSONObject jsonParams;

    /**
     *
     * @param params
     * key 有page,limit,params
     */
    public PageInfo(Map<String, Object> params){
        //分页参数
        Object pageObj = params.get("page");
        Object limitObj = params.get("limit");
        if (pageObj != null && limitObj != null){
            this.page = Integer.parseInt(pageObj.toString());
            this.limit = Integer.parseInt(limitObj.toString());
            this.offset = (page - 1) * limit;
        }
        JSONObject jsonData = JSONObject.parseObject(JSON.toJSONString(params.get("params")));
        this.jsonParams = jsonData == null ? new JSONObject():jsonData;

    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public JSONObject getJsonParams() {
        return jsonParams;
    }

    public void setJsonParams(JSONObject jsonParams) {
        this.jsonParams = jsonParams;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Override
    public String toString() {
        return "page-" + page + "-limit-" + limit + "-offset-" + offset + "-jsonParams-" + jsonParams.toJSONString();
    }
}
